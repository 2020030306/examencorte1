package com.example.banco;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CuentaBancoActivity extends AppCompatActivity {
    private EditText etCantidad;
    private Button btnDepositar, btnRetirar, btnRegresar;
    private TextView tvSaldo;
    private TextView lblUsuario;
    private TextView lblSaldo;
    private CuentaBanco cuenta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_banco);
        iniciarComponentes();
        // Obtener los datos del Main Activity
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");
        String saldo = datos.getString("saldo");
        lblUsuario.setText("Nombre del Cliente: " + nombre);
        lblSaldo.setText("Saldo: $" + saldo);
        Double total = Double.parseDouble(saldo);
        cuenta = new CuentaBanco("", total);

        btnDepositar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etCantidad.getText().toString().matches("")) {
                    Toast.makeText(CuentaBancoActivity.this, "Ingrese la cantidad", Toast.LENGTH_LONG).show();
                } else {
                    double cantidad = Double.parseDouble(etCantidad.getText().toString());
                    cuenta.depositar(cantidad);
                    actualizarSaldo();
                    Toast.makeText(CuentaBancoActivity.this, "Se realizó el depósito", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnRetirar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etCantidad.getText().toString().matches("")) {
                    Toast.makeText(CuentaBancoActivity.this, "Ingrese la cantidad", Toast.LENGTH_LONG).show();
                } else {
                    double cantidad = Double.parseDouble(etCantidad.getText().toString());
                    boolean exito = cuenta.retirar(cantidad);
                    if (exito) {
                        actualizarSaldo();
                        Toast.makeText(CuentaBancoActivity.this, "Se realizó el retiro", Toast.LENGTH_SHORT).show();
                    } else {
                        actualizarSaldo();
                    }
                }
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
    }
    private void iniciarComponentes() {
        etCantidad = findViewById(R.id.txtCantidad);
        btnDepositar = findViewById(R.id.btnDepositar);
        btnRetirar = findViewById(R.id.btnRetirar);
        btnRegresar = findViewById(R.id.btnRegresar);
        tvSaldo = findViewById(R.id.lblSaldo);
        lblUsuario = findViewById(R.id.lblUsuario);
        lblSaldo = findViewById(R.id.lblSaldo);
    }

    private void actualizarSaldo() {
        tvSaldo.setText("Saldo actual: $" + cuenta.getSaldo());
    }

    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Banco SOMEX");
        confirmar.setMessage("¿Regresar al MainActivity?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // No hace nada
            }
        });

        confirmar.show();
    }
}

