package com.example.banco;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtNombre;
    private EditText txtNumCuenta;
    private EditText txtSaldo;
    private Button btnEntrar;
    private Button btnSalir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entrar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    private void iniciarComponentes() {
        txtNumCuenta = (EditText) findViewById(R.id.txtNumCuenta);
        txtSaldo = (EditText) findViewById(R.id.txtSaldo);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnSalir = (Button) findViewById(R.id.btnSalir);
    }

    private void entrar() {

        if(!txtNumCuenta.getText().toString().isEmpty() && !txtSaldo.getText().toString().isEmpty() && !txtNombre.getText().toString().isEmpty()) {
            String nombre = txtNombre.getText().toString();
            String saldo = txtSaldo.getText().toString();

            // Hacer el paquete para enviar información
            Bundle bundle = new Bundle();
            bundle.putString("nombre", txtNombre.getText().toString());
            bundle.putString("saldo", txtSaldo.getText().toString());

            // Crear el intent para llamar a otra actividad
            Intent intent = new Intent(MainActivity.this, CuentaBancoActivity.class);
            intent.putExtras(bundle);

            // Iniciar la actividad esperando o no respuesta
            startActivity(intent);

        }else{
            Toast.makeText(this.getApplicationContext(), "Introduzca todos los datos", Toast.LENGTH_SHORT).show();
        }
    }

    private void salir() {
        android.app.AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Banco SOMEX");
        confirmar.setMessage("¿Salir de la app?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // No hace nada
            }
        });

        confirmar.show();
    }
}

