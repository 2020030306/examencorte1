package com.example.banco;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class CuentaBanco extends AppCompatActivity {
        private String numCuenta;
        private double saldo;

        public CuentaBanco(String numCuenta, Double saldo) {
            this.numCuenta = numCuenta;
            this.saldo = saldo ;
        }

        public void depositar(double cantidad) {
            saldo += cantidad;
        }

        public boolean retirar(double cantidad) {
            if (cantidad <= saldo) {
                saldo -= cantidad;
            } else {
                System.out.println("Saldo insuficiente");
            }
            return false;
        }

        public double getSaldo() {
            return saldo;
        }
    }

